var uniqueId = "";
const loginButton = "body > div > div > div > div.login-container > div > div > div > form > input";
const searchLink = "#main-nav > div > ul > li.searchmenuitem.menuitem > a";
const patientSearchLink = "#main-nav > div > ul > li.searchmenuitem.menuitem > ul > li:nth-child(1) > a";
const patientSearchButton = "#patientSearch";
const patientViewEditButton = "#patientSearchGrid > div.k-grid-content > table > tbody > tr.k-alt > td:nth-child(7) > a.btn.btn-sm.btn-warning.has-tooltip";
const faxButton = "#SynopsisGrid > div.k-grid-content > table > tbody > tr:nth-child(1) > td:nth-child(2) > span > input";
const waitTimeout = 10000;
const waitInterval = 5000;
const initialStatus = "queued";
const completedStatus = "success";


before(() => {
    // generate unique id
    uniqueId = Cypress._.random(0, 1e6);
})

describe('Create a synopisis and send fax', function(){
    it('Should log in', function(){
        cy.visit(Cypress.env('baseUrl'));
        cy.get('[name=UserName]').type(Cypress.env('user').name);
        cy.get('[name=Password]').type(Cypress.env('user').password, { log: false });
        cy.get(loginButton).click();
        cy.url().should('include', '/Account/Users');
    
    }) 
 
    it('Should search for a patient to create synopsis for', function(){
       
         cy.get(searchLink).click();
         cy.get(patientSearchLink).click();
         cy.url().should('include', '/Dashboard');  
         cy.get('[name=LastName]').type(Cypress.env('patient').lastName);
         cy.get('[name=FirstName]').type(Cypress.env('patient').firstName);
         cy.get('[name=SelectedFacilityId]').type(Cypress.env('patient').facilityId);
         cy.get(patientSearchButton).click();
         cy.get(patientViewEditButton).click();
       
      } )

    it('Should create a patient synopsis', function(){
      cy.contains("Viewing " + (Cypress.env('patient').lastName + ", " + Cypress.env('patient').firstName));
      cy.get('#partialVisitSynopsis-tab > div').click();
      cy.get('#partialVisitSynopsis > p > a').click();
      cy.get('#synopsisTemplate').select(Cypress.env('patient').synopsis.template);
     
      cy.get('#SynopsisDetail_Description').clear().type(Cypress.env('patient').synopsis.description + "_" + uniqueId);
      cy.get('#synopsisSave').click()
        
    } )

    it('Should send the created synopsis in a fax', function(){
        // This wait is necessary since the synopsis grid javascripts takes a while to load
        cy.wait(waitTimeout);
        cy.document().then((doc) => {
            doc.querySelector(faxButton).click();
        })
        cy.get('#faxRecipients_chosen > ul > li > input').type('{enter}' + Cypress.env('patient').synopsis.recipientFaxNumber);
        cy.get('#senderPhone').clear().type(Cypress.env('patient').synopsis.senderPhone);
        cy.get('#sender').clear().type(Cypress.env('patient').synopsis.sender + "_" + uniqueId);
        cy.get('#coverMessage').clear().type(Cypress.env('patient').synopsis.coverMessage); 
        cy.get('#subject').clear().type(Cypress.env('patient').synopsis.faxSubject); 
        cy.get('#senderFax').clear().type(Cypress.env('patient').synopsis.senderFax); 
        cy.get('#sendFax').click();
        cy.get("#SynopsisGrid > div.k-grid-content > table > tbody > tr:has(td:contains('" + uniqueId + "'))").should('be.visible');


    } )
 
    it('Should verify in the outbound fax log page that the fax was sent successfully', function(){
       
        cy.visit(Cypress.env('baseUrl'));
        cy.get('#main-nav > div > ul > li.reportingmenuitem.menuitem > a').click();
        cy.get('#main-nav > div > ul > li.reportingmenuitem.menuitem > ul > li:nth-child(3) > a').click();
        cy.url().should('include', 'Reports/OutboundFaxLog');  
        cy.get('#FacilityDropDown').select(Cypress.env('patient').facilityId);
        // Wait for 1 minute and 30 seconds
        cy.wait(waitTimeout * 9);
        cy.get('#runReport').click();
        cy.get('#OutboundFaxLogGrid > div.k-grid-content > table > tbody > tr:nth-child(1) > td:nth-child(17) > span').should('have.text', completedStatus);
      

     } ) 

     it('Should Log out', function(){
        cy.visit(Cypress.env('baseUrl'))
        cy.get("#btnSignOut").click() 
    })  

    
})



 