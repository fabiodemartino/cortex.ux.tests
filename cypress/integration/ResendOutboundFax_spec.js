describe('Resend an outbound fax', function(){

    it('Should log in', function(){
        cy.visit(Cypress.env('baseUrl'))
        cy.get('[name=UserName]').type(Cypress.env('user').name)
        cy.get('[name=Password]').type(Cypress.env('user').password, { log: false })
        cy.get('body > div > div > div > div.login-container > div > div > div > form > input').click()
        cy.url().should('include', '/Account/Users')
    
    })

    it('Should resend a fax from the fax log page', function(){
       
      cy.get('#main-nav > div > ul > li.reportingmenuitem.menuitem > a').click()
      cy.get('#main-nav > div > ul > li.reportingmenuitem.menuitem > ul > li:nth-child(3) > a').click()
      cy.url().should('include', 'Reports/OutboundFaxLog')  
        
   } )
   
   it('Should Log out', function(){
    cy.visit(Cypress.env('baseUrl'))
    cy.get("#btnSignOut").click() 
   })

})